import { Component, OnInit, AfterViewInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { ReportsService } from 'src/app/services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit, AfterViewInit {
  user: User;
  menu: boolean;
  isNav: string;
  barChartOptions = {
    responsive: true,
    fill: true
  };

  public barChartLabels = [];
  public barChartLegend = true;
  public chartData = [];
  isLoaded = false;

  constructor(private reportsSvc: ReportsService) {
    this.isNav = 'navFalse';
   }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.createChart();
    this.createPayments();
  }
  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }

  createChart() {
    const arr = [];
    this.reportsSvc.getUsersByMonth().subscribe((docs: any) => {
      docs.forEach(element => {
        this.barChartLabels.push(element.month);
        arr.push(element.users.length);
        this.chartData.push({data: arr, label: 'Usuarios por mes'});
      });
      this.isLoaded = true;
    });
  }

  createPayments() {
    const arr = [];
    this.reportsSvc.getPaymentsByMonth().subscribe((docs: any) => {
      console.log(docs);
      // docs.forEach(element => {
      //   this.barChartLabels.push(element.month);
      //   arr.push(element.users.length);
      //   this.chartData.push({data: arr, label: 'Usuarios por mes'});
      // });
      // this.isLoaded = true;
    });
  }
}
