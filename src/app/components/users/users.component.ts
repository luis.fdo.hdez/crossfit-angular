import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort, MatDialog} from '@angular/material';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { NgForm } from '@angular/forms';
import { PaymentComponent } from '../payment/payment.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  menu: boolean;
  isNav: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource<any>();
  users;
  user: User;
  selectedRole: string;
  roles = [{value: 'client', viewValue: 'Crossfiter'}, {value: 'manager', viewValue: 'Trainer'}];
  displayedColumns = ['name', 'email', 'createdAt', 'deathLine', 'actions'];
  constructor(public userSvc: UserService, public dialog: MatDialog) {
    this.user = new User();
    this.isNav = 'navFalse';
  }

  ngOnInit() {
    this.getUsers();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    console.log(filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }

  getUsers() {
    this.userSvc.getUsers().subscribe(users => {
      this.users = users;
      this.dataSource = new MatTableDataSource(this.users);
    });
  }

  register(form: NgForm) {
    this.userSvc.createUser(form.value).subscribe(registered => {
      this.getUsers();
    });
  }

  openPayment(user) {
    const dialogRef = this.dialog.open(PaymentComponent, {
      width: '500px',
      data: user
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getUsers();
    });
  }
}
