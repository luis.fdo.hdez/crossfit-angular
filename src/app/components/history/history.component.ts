import { Component, OnInit } from '@angular/core';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  dones;
  isNav: string;
  notAttendance;
  displayedColumns = ['Date', 'Time'];
  constructor(private schedSvc: ScheduleService) {
    this.isNav = 'navFalse';

    this.dones = [];
    this.notAttendance = [];
  }

  ngOnInit() {
    this.getDone();
    this.getNotAtt();
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }

  getDone() {
    this.schedSvc.getHistory().subscribe(dones => {
      this.dones = dones;
    });
  }

  getNotAtt() {
    this.schedSvc.getUnnatendance().subscribe(lacks => {
      this.notAttendance = lacks;
    });
  }
}
