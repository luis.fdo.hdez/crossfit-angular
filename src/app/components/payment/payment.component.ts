import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from 'src/app/models/user.model';
import * as moment from 'moment';
import { PaymentService } from 'src/app/services/payment.service';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  deathLine;
  today;
  amount: number;
  constructor(public dialogRef: MatDialogRef<PaymentComponent>,
              @Inject(MAT_DIALOG_DATA) public user: User, public paymentSvc: PaymentService) { }

  ngOnInit() {
    this.chooseDeathLine();
  }

  chooseDeathLine() {
    const tmpDeathLine = moment().add(30, 'days').format('YYYY/MM/DD');
    console.log(tmpDeathLine);
    this.deathLine = new Date(tmpDeathLine);
  }

  close() {
    this.dialogRef.close();
  }

  capturePayment() {
    const data = {
      clientId: this.user._id,
      amount: this.amount,
      deathLine: this.deathLine
    };
    this.paymentSvc.postPayment(data).subscribe(done => {
      this.close();
    });
  }
}
