import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';

declare var $: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  oldPassword: string;
  newPassword: string;
  menu: boolean;
  isNav: string;
  constructor(private userSvc: UserService) {
    this.isNav = 'navFalse';
   }

  ngOnInit() {
    this.userSvc.verify().subscribe(user => {
      this.user = user as User;
    });
  }

  changePass() {
    const data = {
      oldPassword: this.oldPassword,
      newPassword: this.newPassword
    };
    this.userSvc.changePass(data).subscribe(changed => {
      $('#passwordModal').modal('hide');
      this.oldPassword = '';
      this.newPassword = '';
    });
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }
}
