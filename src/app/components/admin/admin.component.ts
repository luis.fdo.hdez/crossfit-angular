import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  user: User;
  menu: boolean;
  isNav: string;
  deviceToken: string;

  constructor(private router: Router,
              public userSvc: UserService,
              private snackBar: MatSnackBar) {
                this.isNav = 'navFalse';
                this.user = new User();
               }

  ngOnInit() {
    this.verifyLoggedIn();
  }

  verifyLoggedIn() {
    this.userSvc.verify().subscribe(user => {
      this.user = user;
      console.log(this.user);
    });
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }
}
