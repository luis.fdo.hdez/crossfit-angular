import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { MatDatepickerInputEvent } from '@angular/material';
import * as moment from 'moment';
import { UserService } from 'src/app/services/user.service';
import { ScheduleService } from 'src/app/services/schedule.service';
import { Schedule } from 'src/app/models/schedule.model';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  user: User;
  menu: boolean;
  isNav: string;
  today = new Date();
  minDate = this.today;
  selectedDate;
  isScheduled: boolean;
  selectedHorario: string;
  isReady: boolean;
  ocurrence;
  existingDate: boolean;
  maxDate = new Date(this.today.getFullYear(), this.today.getMonth() + 1, 0);
  isComplete: boolean;
  scheduleds: Array<any>;
  horarios = ['8:00 am - 9:00 am',
              '9:00 am - 10:00 am',
              '10:00 am - 11:00 am',
              '11:00 am - 12:00 pm',
              '5:00 pm - 6:00 pm',
              '6:00 pm - 7:00 pm',
              '7:00 pm - 8:00 pm',
              '8:00 pm - 9:00 pm',
              '9:00 pm - 10:00 pm'];

  constructor(public userSvc: UserService, public schedSvc: ScheduleService) {
    this.isNav = 'navFalse';
    this.user = new User();
    this.existingDate = false;
    this.isComplete = true;
    this.scheduleds = [];
  }

  ngOnInit() {
    this.verify();
  }

  selectDate(input: string, event: MatDatepickerInputEvent<Date>) {
    this.isScheduled = true;
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }

  verify() {
    this.userSvc.verify().subscribe(user => {
      this.user = user as User;
      this.getSchedules();
    }, (err) => {
    });
  }

  getSchedules() {
      this.schedSvc.getMySchedules(this.user._id).subscribe(docs => {
        this.scheduleds = docs as Schedule[];
      });
  }

  readyToGo(event) {
    this.ocurrence = undefined;
    this.isComplete = false;
  }

  agendar() {
    const data = {
      date: moment(this.selectedDate).format('YYYY/MM/DD'),
      time: this.selectedHorario.toString()
    };
    this.scheduleds.forEach(element => {
      if (element.date === data.date) {
        this.ocurrence = element;
      }
    });
    if (this.ocurrence) {
      // hacer un put
      const index = this.scheduleds.indexOf(this.ocurrence);
      const oldSched = this.scheduleds[index];
      console.log(oldSched);
      const putData = {
        _id: oldSched._id,
        client: oldSched.client,
        date: data.date,
        time: data.time
      };
      this.schedSvc.editSchedule(putData).subscribe(updated => {
        this.getSchedules();
      });
      // this.scheduleds.splice(index, 1);
      // this.scheduleds.push(data);
      // console.log(this.scheduleds);
    } else {
      // crear uno nuevo
      this.schedSvc.createSchedule(data).subscribe(created => {
        this.getSchedules();
      });
    }
  }
}
