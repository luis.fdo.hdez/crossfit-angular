import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent implements OnInit {
 // tslint:disable-next-line:variable-name
 private _user;
 get user(): any {
   return this._user;
 }

 @Input()
 set user(val: any) {
   this._user = val;
 }

 @Output() isNav = new EventEmitter<boolean>();
 menu: boolean;

 constructor(private router: Router, public userSvc: UserService) { }

 ngOnInit() {
 }

 toggleMenu() {
   this.menu = !this.menu;
   this.isNav.emit(this.menu);
 }

 logout() {
   this.userSvc.logout().subscribe(logout => {
     if (logout === false) {
       this.router.navigate(['/']);
     }
   });
 }
}
