import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  user: User;
  constructor(public dialogRef: MatDialogRef<LoginComponent>,
              private router: Router,
              public userSvc: UserService) {
    this.user = new User();
   }

  ngOnInit() {
  }

  close(): void {
    this.dialogRef.close();
  }

  loginClient() {
    const data = {
      email: this.user.email,
      password: this.user.password
    };
    this.userSvc.loginClient(data).subscribe(user => {
      this.close();
      this.router.navigate(['home']);
    }, (err) => {
      console.log(err);
    });
  }

  loginTrainer() {
    const data = {
      email: this.user.email,
      password: this.user.password
    };
    this.userSvc.loginTrainer(data).subscribe(trainer => {
      this.close();
      this.router.navigate(['admin']);
    });
  }
}
