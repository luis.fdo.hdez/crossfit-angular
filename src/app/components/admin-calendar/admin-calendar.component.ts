import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
  selector: 'app-admin-calendar',
  templateUrl: './admin-calendar.component.html',
  styleUrls: ['./admin-calendar.component.scss']
})
export class AdminCalendarComponent implements OnInit {
  isNav: string;
  selectedDate;
  dataSource;
  displayedColumns = ['name', 'date', 'time'];
  constructor(public schedSvc: ScheduleService) {
    this.isNav = 'navFalse';
    this.dataSource = [];
  }

  ngOnInit() {
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }
  selectDate() {
  }

  search() {
    const date = moment(this.selectedDate).format('YYYY-MM-DD');
    this.schedSvc.getAdminSchedules(date).subscribe(schedules => {
      this.createTable(schedules);
    });
  }

  createTable(scheduleds) {
    this.dataSource = [];
    this.dataSource = scheduleds;
  }
}
