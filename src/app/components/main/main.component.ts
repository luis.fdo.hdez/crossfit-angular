import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { ScheduleService } from 'src/app/services/schedule.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  user: User;
  menu: boolean;
  isNav: string;
  deviceToken: string;
  hours;

  constructor(private router: Router,
              public userSvc: UserService,
              private snackBar: MatSnackBar,
              private schedSvc: ScheduleService) {
                this.isNav = 'navFalse';
                this.user = new User();
               }

  ngOnInit() {
    this.verifyLoggedIn();
    this.getDone();
  }

  verifyLoggedIn() {
    this.userSvc.verify().subscribe(user => {
      this.user = user;
    });
  }

  getDone() {
    this.schedSvc.getHistory().subscribe(dones => {
      this.hours = dones;
    });
  }

  toggleNav(event) {
    if (event === true) {
      this.isNav = 'navTrue';
    } else {
      this.isNav = 'navFalse';
    }
  }

}
