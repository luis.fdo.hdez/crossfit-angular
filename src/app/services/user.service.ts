import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  createUser(data) {
    return this.http.post(environment.localUrl + '/api/users/new', data);
  }

  loginClient(data) {
    return this.http.post(environment.localUrl + '/api/auth/client', data);
  }

  loginTrainer(data) {
    return this.http.post(environment.localUrl + '/api/auth/trainer', data);
  }

  verify(): Observable<any> {
    return this.http.get(environment.localUrl + '/api/auth/verify');
  }

  verifySchedules() {
    return this.http.get(environment.localUrl + '/api/auth/updateSchedule');
  }

  logout() {
    return this.http.get(environment.localUrl + '/api/auth/logout');
  }

  changePass(data) {
    return this.http.put(environment.localUrl + `/api/users/changePass`, data);
  }

  getUsers() {
    return this.http.get(environment.localUrl + '/api/users/all');
  }
}
