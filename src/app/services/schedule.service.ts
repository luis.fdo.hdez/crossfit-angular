import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(private http: HttpClient) { }

  createSchedule(data) {
    return this.http.post(environment.localUrl + '/api/schedule/new', data);
  }

  getMySchedules(id: string) {
    return this.http.get(environment.localUrl + `/api/schedule/all/${id}`);
  }

  editSchedule(data) {
    return this.http.put(environment.localUrl + `/api/schedule/${data._id}`, data);
  }

  getHistory() {
    return this.http.get(environment.localUrl + '/api/schedule/history');
  }

  getUnnatendance() {
    return this.http.get(environment.localUrl + '/api/schedule/noattendance');
  }

  getAdminSchedules(date) {
    return this.http.get(environment.localUrl + `/api/schedule/usersByDate/${date}`);
  }
}
