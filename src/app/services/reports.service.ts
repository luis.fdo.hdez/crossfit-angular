import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http: HttpClient) { }

  getUsersByMonth() {
    return this.http.get(environment.localUrl + '/api/reports/usersByMonth');
  }

  getPaymentsByMonth() {
    return this.http.get(environment.localUrl + '/api/reports/paymentsByMonth');
  }
}
