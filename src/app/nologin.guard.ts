import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class NologinGuard implements CanActivate {
  user;
  constructor(private userSvc: UserService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      this.userSvc.verifySchedules().subscribe(user => {
        this.user = user;
      });
      if (this.user === null || this.user === undefined) {
        return true;
      } else {
        this.router.navigate(['home']);
        return false;
      }
  }

}
