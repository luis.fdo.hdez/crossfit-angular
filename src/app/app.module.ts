import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingComponent } from './components/landing/landing.component';
import { LandingNavComponent } from './components/landing-nav/landing-nav.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { MaterialModule } from './material.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './services/user.service';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { MatDatepickerModule } from '@angular/material';
import { ScheduleService } from './services/schedule.service';
import { HistoryComponent } from './components/history/history.component';
import { LoginGuard } from './login.guard';
import { NologinGuard } from './nologin.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { UsersComponent } from './components/users/users.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PaymentService } from './services/payment.service';
import { ReportsComponent } from './components/reports/reports.component';
import { ChartsModule } from 'ng2-charts';
import { ReportsService } from './services/reports.service';
import { AdminCalendarComponent } from './components/admin-calendar/admin-calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LandingNavComponent,
    LoginComponent,
    MainComponent,
    SidebarComponent,
    CalendarComponent,
    HistoryComponent,
    ProfileComponent,
    AdminComponent,
    AdminSidebarComponent,
    UsersComponent,
    PaymentComponent,
    ReportsComponent,
    AdminCalendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ChartsModule,
  ],
  providers: [UserService, MatDatepickerModule, ScheduleService, LoginGuard, NologinGuard,
              PaymentService, ReportsService],
  entryComponents: [LoginComponent, PaymentComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
