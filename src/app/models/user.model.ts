export class User {
  // tslint:disable-next-line:variable-name
  _id?: string;
  name: string;
  email: string;
  password: string;
  role: string;
  phone?: string;
  deathLine?: string;
  createdAt: string;
  updatedAt: string;
}
