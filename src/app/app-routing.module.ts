import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';
import { MainComponent } from './components/main/main.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { HistoryComponent } from './components/history/history.component';
import { LoginGuard } from './login.guard';
import { NologinGuard } from './nologin.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminComponent } from './components/admin/admin.component';
import { UsersComponent } from './components/users/users.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AdminCalendarComponent } from './components/admin-calendar/admin-calendar.component';

const routes: Routes = [
  { path: '', component: LandingComponent, canActivate: [NologinGuard] },
  { path: 'home', component: MainComponent}, // , canActivate: [LoginGuard]
  { path: 'calendar', component: CalendarComponent}, // , canActivate: [LoginGuard]
  { path: 'history', component: HistoryComponent}, // , canActivate: [LoginGuard]
  { path: 'profile', component: ProfileComponent},
  { path: 'admin', component: AdminComponent},
  { path: 'usuarios', component: UsersComponent},
  { path: 'reportes', component: ReportsComponent},
  { path: 'admin-calendar', component: AdminCalendarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
